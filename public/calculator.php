<?php
$age = $_POST['age'];
$uncertainty = $_POST['uncertainty'];

$csv = array_map('str_getcsv', file('c14.csv')); 
$arr = [];

foreach($csv as $row){
    if($row[1] > $age - 2*$uncertainty & $row[1] < $age + 2*$uncertainty) {
        array_push($arr, $row);
    }
}

if(count($arr) > 0) {

    $minCalibratedDate = $arr[0][0];
    $maxCalibratedDate = $arr[0][0];
    
    foreach($arr as $row) {
        if($row[0] < $minCalibratedDate) {
            $minCalibratedDate = $row[0];
        }
        if($row[0] > $maxCalibratedDate) {
            $maxCalibratedDate = $row[0];
        }
    }

    $data = [
        "status" => "Successful",
        "message" => "",
        "min" => $minCalibratedDate,
        "max" => $maxCalibratedDate
    ];

    echo(json_encode($data));

} else {
    $data = [
        "status" => "Error",
        "message" => "No corresponding dates.",
    ];
    
    echo(json_encode($data));
}

?>
