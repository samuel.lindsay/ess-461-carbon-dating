function CalibrateDate() {
    inputtedDate = document.getElementById("age").valueAsNumber;
    inputtedUncertainty = document.getElementById("uncertainty").valueAsNumber;

    if(inputtedDate > 12000) {
        alert("Warning: this tool is not designed to calibrate carbon-14 dates older than 12,000 years BP.")
    } else if(inputtedDate < 0) {
        alert("Only positive ages are allowed.")
        return;
    }

    var data = {
        age : inputtedDate,
        uncertainty : inputtedUncertainty
    }

    $.ajax({
        type: 'POST',
        url: 'calculator.php',
        data: data,
        dataType: 'json',
        success: function(response) {

            console.log(response);

            if(response["status"] == "Error") {
                alert(response["message"]);
            } else {
                document.getElementById("minDate").innerText = response["min"]
                document.getElementById("maxDate").innerText = response["max"]
            }
        }
    })
}